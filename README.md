# La phase "compliquée" commence ici !

Incroyable cet ajout de lignes dans le fichier readme.

Vous venez de merger la branche "improve-doc" dans master.

Le travail sur les branches "ajout-styles" et "modification-textes" est terminé. Vous souhaitez donc logiquement le rapatrier sur la branche master.

Mais surprise ! La branche master a changé depuis la dernière fois... Vous pourriez utiliser la commande merge, mais vous vous souvenez du cours sur le rebase...